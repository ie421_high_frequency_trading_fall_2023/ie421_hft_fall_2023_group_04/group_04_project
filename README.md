# Immersive 3D Visualization: Unveiling the Mystique of GNSS and the Satellites that Power them

The report can be found in the REPORT.md file 


## Installation and Dependencies
- Install Libraries: 
    - ```pip3 install -r requirements.txt```
- Setup MySQL instance and .env file
- Run Database Setup file
    - ```python3 db_setup.py```
- Run Project (api service + GNSS receiver data collection + frontend) 
    - run ```./run_gps_api.sh```


## Testing
- run ```./run_gps_api.sh```
- Navigate to the FastAPI docs ```http://127.0.0.1:5003/docs```



## People
- Akash Sannasi akashsan522@gmail.com
- Harshda Ghai hghai2@illinois.edu
- Ananya Agarwal ananyaa7@illinois.edu
- Vashishth Goswami vgosw2@illinois.edu



